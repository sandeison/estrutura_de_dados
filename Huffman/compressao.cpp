#include "compressao.h"
#include <QDebug>
#include "QMessageBox"
#include<QByteArray>


Compressao::Compressao()
{


}

void Compressao::compress(QString endEntrada,QString endSaida ) {

       //Obtem as informações do arquivo de entrada
       infoArquivoEnt.setFile(endEntrada);

       // Nome do Arquivo
       nomeB = infoArquivoEnt.fileName();

       // Tamanho do nome do Arquivo
       tNomeOriginalB = nomeB.size();

       //Leitura do arquivo é passado para novoArquivo
       leArquivo(endEntrada);

       //Frequencia do Arquivo
       frequencia();

       // Metodo para criar a arvore
       arvore();

       if (lista.size()!= 0){ // Verifica se a lista esta vazia
           setNewCodes(); // Cria a Representação binaria de cada valor
       }

       // crio a representação binaria e a tabela hash
       representacao(&lista[0]);

       // Tamanho da Arvore
       tArvoreB = represArvoreB.size();

       // Codifico o arquivo em Bytes
       codifica();

       codificabits();

       codificaint( tlixoB, tArvoreB, tNomeOriginalB);


       escreveArquivo.append(tlixoArvoreNome);
       escreveArquivo.append(nomeB);
       escreveArquivo.append(represArvoreB);
       escreveArquivo.append(codificacao);

       qDebug()<<"tlixoB "<< tlixoB << endl ;
       qDebug()<<"tArvoreB "<< tArvoreB << endl ;
       qDebug()<<"tNomeOriginalB "<< tNomeOriginalB << endl ;







       //Metodos para Gravar o Arquivo
       arquivo.setFileName(endSaida);
       if ( arquivo.open (QIODevice::WriteOnly)) {
           arquivo.write(escreveArquivo);
       }
       arquivo.close();

}

// Abre o arquivo e faz uma copia para o nevoArquivo
void Compressao::leArquivo(QString endEntrada){

    arquivo.setFileName(endEntrada);
    if(arquivo.open (QIODevice::ReadOnly))
    {
        // Faço uma copia do arquivo original par a um arquivo temporario
        novoArquivo = arquivo.readAll();
    }
    arquivo.close();
}



// Verifica a frequencia do arquivo
void Compressao::frequencia(){
    //Frequencia do Arquivo
    int freq[256];
    // Contanto a frequencia
    for(int i = 0; i < 256; i++){
        freq[i] = novoArquivo.count(i);
        if (freq[i]!=0){
            //Criando um NO
            Node *tmp = new Node;
            tmp->character.append(i);
            tmp->frequencia = freq[i];
            // PASSO O NO PARA A LISTA
            lista << * tmp;
        }
    }
}


// Cria a Arvore
void Compressao::arvore(){
    while(lista.size() > 1){
        //Ordena a Lista
        sortList();
        Node *tmp = new Node;
        tmp->character.append('*');
        tmp->frequencia = lista[0].frequencia + lista[1].frequencia;

        Node *tmp0 = new Node;
        Node *tmp1 = new Node;

        tmp0->character.append(lista[0].character);
        tmp0->frequencia=lista[0].frequencia;
        tmp0->left = lista[0].left;
        tmp0->right = lista[0].right;

        lista.removeFirst();

        tmp1->character.append(lista[0].character);
        tmp1->frequencia=lista[0].frequencia;
        tmp1->left = lista[0].left;
        tmp1->right = lista[0].right;

        lista.removeFirst();

        tmp->left = tmp0;
        tmp->right = tmp1;

        // CRIO UM NOVO NO COM A SOMA DOS DOIS NOS COM MENORES FREQUENCIAS
        lista << * tmp;
    }
}

// Ordena a Lista
void Compressao::sortList(){
    bool troca = true;
    if(lista.size() == 1){
        return;
    }
    while(troca)
    {
      troca = false;
      for(int i = 1; i < lista.size(); ++i)
        if(lista[i].frequencia < lista[i - 1].frequencia){
           lista.swap(i, i-1);
           troca = true;
        }
    }

}

void Compressao::setNewCodes()
{
    QByteArray nulo;
    setNewCodes(&lista[0], nulo);
}

void Compressao::setNewCodes(Node * current, QByteArray key)
{
    if(isleaf(current))
    {
        current->novocharacter = key;

    }
    else
    {
        QByteArray keyLeft = key + '0';
        setNewCodes(current->left, keyLeft);

        QByteArray keyRight = key + '1';
        setNewCodes(current->right, keyRight);
    }
}

bool Compressao::isleaf(Node * tmp)
{
    return(!tmp->left);
}

void Compressao::representacao(Node *aux){
        //Crio a representação da arvore

        if (aux[0].left==NULL && aux[0].right==NULL && (aux[0].character == "*" || aux[0].character == "/" ) ){
             represArvoreB.append('/');
        }

        represArvoreB.append(aux[0].character);

        if (aux[0].left==NULL && aux[0].right==NULL ){
            hash.insert(aux[0].character,aux[0].novocharacter );
        }

        if(aux[0].left!=NULL){
            representacao(aux[0].left);
        }
        if(aux[0].right!=NULL){
            representacao(aux[0].right);
        }
}

void Compressao::codifica(){

    QString valor; // Recebe o valor do arqivo tranformando em string
    int total=0;
    QBitArray bits(8);

    for(int j = 0; j < novoArquivo.size();j++){
        valor = novoArquivo[j];
        QHashIterator<QString, QByteArray> i(hash);
        while (i.hasNext()) {
             i.next();
             if(valor==i.key()){
                codificacaoB.append(i.value());
                total+=i.value().size();
                if(total>8){
                    total-=8;
                }
                break;
             }
        }

    }

    // Tamanho do Lixo
    tlixoB = 8 - total;

    //Criamos o Lixo com 0
    for(int i = 0; i < tlixoB; i++){
       codificacaoB.append('0');
    }
}

void Compressao::codificabits(){
    int cont = 0 ;
    QBitArray bits(8);

    for(int i = 0; i < codificacaoB.size(); i++){
        if(codificacaoB[i]=='0'){
          bits[cont]=0;
        }else{
          bits[cont]=1;
        }
        cont++;
        if(cont==8){
          cont=0;
          //Converter os bits em Bytes
          QByteArray bytes;
          bytes.resize(bits.count()/8);
          bytes.fill(0);
          // Convert from QBitArray to QByteArray
          for(int b=0; b<bits.count(); ++b){
             bytes[b/8] = ( bytes.at(b/8) | ((bits[b]?1:0)<<(b%8)));
          }
        //Inserindo os Bytes
        codificacao.append(bytes);
       }
    }
}

void Compressao::codificaint(int a, int b, int c){
    QBitArray bits(24);
    int cont = 0;
    bits.fill(0);

    if(b<=8191){
        cont=2;
        while(a > 0){
           bits[cont]= a % 2;
           a/=2;
           cont--;
        }

        cont = 15;
        while(b > 0){
           bits[cont]= b % 2;
            b/=2;
            cont--;
        }

        cont =23;
        while(c > 0){
           bits[cont]= c % 2;
            c/=2;
            cont--;
        }

        QByteArray bytes;
        bytes.resize(bits.count()/8);
        bytes.fill(0);
        // Convert from QBitArray to QByteArray
        for(int j=0; j<bits.count(); ++j)
            bytes[j/8] = ( bytes.at(j/8) | ((bits[j]?1:0)<<(j%8)));

        tlixoArvoreNome.append(bytes);

    }

}


Compressao::~Compressao()
{
    //lista.clear();
}
