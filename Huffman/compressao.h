#ifndef COMPRESSAO_H
#define COMPRESSAO_H


#include<QString>
#include <QFileInfo>
#include <QFile>
#include <node.h>
#include<QHash>
#include<QBitArray>
#include<QByteArray>

class Compressao
{
public:
    Compressao();

    //Metodo que recebe os endereços de entrada e saida
    void compress(QString endEntrada, QString endSaida);

    //Metodo para Leitura o arquivo
    void leArquivo(QString endEntrada);

    // Metodo para a frequencia do arquivo
    void frequencia();

    // Cria a Arvore
    void arvore();

    // Ordena a Lista
    void sortList();

    // Cria a representação Binaria da arvore
    void setNewCodes();

    void setNewCodes(Node * current, QByteArray key);

    bool isleaf(Node *tmp);

    // Representação da Arvore e Cria a tabela Hash
    void representacao(Node *aux);

    //CRIA A REPRESENTAÇÃO EM BYTES
    void codifica();

    void codificabits();

    void codificaint(int a, int b, int c);

    //Objeto para buscar informação do arquivo
    QFileInfo infoArquivoEnt; // OK

    // Objeto para manipular os dados
    QFile arquivo; //OK

    // Objeto para copiar o arquivo principal, onde podemos manipular sem perder o dado origianl.
    QByteArray novoArquivo; // OK

    // Objeto para criar a lista
    QList<Node> lista; // OK

    //Tabela Hash
    QHash<QString, QByteArray> hash;

    // CABEÇALHO EM BYTES

    // Tamanho do Lixo
    int tlixoB;

    // Tamanho da Arvore
    int tArvoreB;

    // Tamanho do  Nome do Arquivo
    int tNomeOriginalB; // OK

    // Nome do arquivo
    QString nomeB;  // OK

    //Representação da arvore
    QByteArray represArvoreB;

    // Codificação do arquivo em bytes
    QByteArray codificacaoB;

    // Codificação do arquivo em bits
    QByteArray codificacao;

    QByteArray tlixoArvoreNome; // Concatenação dos bits

    QByteArray escreveArquivo;

    ~Compressao();
};

#endif // COMPRESSAO_H
