#ifndef DESCOMPRESSAO_H
#define DESCOMPRESSAO_H

#include<QString>
#include <QFileInfo>
#include <QFile>
#include <node.h>
#include<QHash>
#include<QBitArray>


class descompressao
{
public:
    descompressao();

    //Metodo que recebe os endereços de entrada e saida
    void descompress(QString endEntrada, QString endSaida);

    //Metodo para Leitura o arquivo
    void leArquivo(QString endEntrada);

    void separaArquivo();

    void bytesToBits(QByteArray bytes);

    // Cria Lista
    void criaLista(QByteArray cLista);

    // Cria a Arvore
    void arvore();

    // Imprimi a Arvore
    void impArvore(Node *aux, int cont);

    // Objeto para criar a lista
    QList<Node> lista;

    // Dados do arquivo

    //Objeto para buscar informação do arquivo
    QFileInfo infoArquivoEnt;

    QString novoEnd;

    // Objeto para manipular os dados
    QFile arquivo;

    // Objeto para copiar o arquivo principal, onde podemos manipular sem perder o dado origianl.
    QByteArray novoArquivo;

    QByteArray tlixoArvoreNome;

    QByteArray escrever;

    QString nomeOriginal; // Nome do Arquivo

    QByteArray represArvore; //REPRESENTAÇÃO DA ARVORE

    QByteArray codificacao;

    QByteArray codificacao2;

    ~descompressao();
};

#endif // DESCOMPRESSAO_H
