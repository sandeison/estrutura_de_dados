#ifndef NODE_H
#define NODE_H

#include <QByteArray>


class Node {

public:

    int frequencia;
    QByteArray character;
    QByteArray novocharacter;
    Node *left;
    Node *right;

    Node();

};


#endif // NODE_H
