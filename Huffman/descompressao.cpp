#include "descompressao.h"
#include <QDebug>
#include "QMessageBox"
#include<math.h>

descompressao::descompressao()
{

}


void descompressao::descompress(QString endEntrada,QString endSaida ) {

       //Obtem as informações do arquivo de entrada
       infoArquivoEnt.setFile(endEntrada);
       //Leitura do arquivo

       leArquivo(endEntrada);

       separaArquivo();

       criaLista(represArvore);

       arvore();

       int i=0;

       if( codificacao2.size() == 1){

       }else{
           impArvore(&lista[0],i);
       }

       //infoArquivoEnt.setFile(endSaida);

       arquivo.setFileName(endSaida+nomeOriginal);

       if ( arquivo.open (QIODevice::WriteOnly)) {
            arquivo.write(escrever);
        }
        arquivo.close();
}


// Abre o arquivo e faz uma copia para o nevoArquivo
void descompressao::leArquivo(QString endEntrada)
{
    arquivo.setFileName(endEntrada);
    if(arquivo.open (QIODevice::ReadOnly))
    {
        novoArquivo = arquivo.readAll(); // Faço uma copia do arquivo original par a um arquivo temporario
    }


    arquivo.close();
}

// separação do arquivo
void descompressao::separaArquivo(){

    // Separando o tamanho do lixo, arvore e do nome
    tlixoArvoreNome.append(novoArquivo[0]);
    tlixoArvoreNome.append(novoArquivo[1]);
    tlixoArvoreNome.append(novoArquivo[2]);

    QBitArray bits(tlixoArvoreNome.count()*8);
    // Convert from QByteArray to QBitArray
    for(int i=0; i<tlixoArvoreNome.count(); ++i)
       for(int b=0; b<8; ++b)
         bits.setBit(i*8+b, tlixoArvoreNome.at(i)&(1<<b));

    int tamlixo = 0;

    int tamArvore = 0; // Tamanho da Arvore

    int tamNomeOriginal = 0; // tamanho do  nome do arquivo

    //calcula o tamanho do lixo
    int cont=0;
    for(int i = 2; i >=0; i--){
        if(bits[i]==true){
           tamlixo+= pow(2,cont);
        }
        cont++;
    }


    //calcula o tamanho do arvore
    cont =0;
    for(int i = 15; i >=3; i--){
        if(bits[i]==true){
           tamArvore+= pow(2,cont);
        }
        cont++;
    }

    //calcula o tamanho do nome
    cont =0;
    for(int i = 23; i >=16; i--){
        if(bits[i]==true){
           tamNomeOriginal+= pow(2,cont);
        }
        cont++;
    }


    // Nome original
    int total = tamNomeOriginal+3;
    for(int i = 3; i < total; i++){
        nomeOriginal+=novoArquivo[i];
    }

    // Arvore
    int i = total;
    total +=tamArvore ;
    bool achou = false;
    for( i; i < total; i++){
        if(novoArquivo[i]=='/'){
            achou=true;
        }

        if(achou == true){
             i++;
             represArvore.append(novoArquivo[i]);
             achou=false;
        }else{
           represArvore.append(novoArquivo[i]);
        }

    }


    // Codificação
    i = total;
    for( i; i < novoArquivo.size(); i++){
        codificacao.append(novoArquivo[i]);
    }



    QBitArray bits2(codificacao.count()*8);
    // Convert from QByteArray to QBitArray
    for(int i=0; i<codificacao.count(); ++i)
       for(int b=0; b<8; ++b)
         bits2.setBit(i*8+b, codificacao.at(i)&(1<<b));



       for(int i=0; i<bits2.size()-tamlixo; ++i){

          if (bits2[i]==true){
                codificacao2.append('1');
          }else{
              if (bits2[i]==false){
                    codificacao2.append('0');
              }
          }
       }




}

void descompressao::criaLista(QByteArray cLista){

    for(int i = 0; i <cLista.size() ; i++){
        Node * tmp = new Node;
        tmp->character.append(cLista[i]);
        lista << *tmp;
    }


}


//Recrio a Arvore
void descompressao::arvore(){

    while(lista.size() > 1){


        int cont = 0;
        for(int i = lista.size()-1; i >=0 ; i--){
         cont++;
         if( lista[i].character == "*"&& cont>=3 && (lista[i].left==NULL && lista[i].right ==NULL) ) {
             Node *tmp0 = new Node;
             Node *tmp1 = new Node;

             tmp0->character.append(lista[i+1].character);
             tmp0->left = lista[i+1].left;
             tmp0->right = lista[i+1].right;
             lista[i].left = tmp0;
             lista.removeAt(i+1);

             tmp1->character.append(lista[i+1].character);
             tmp1->left = lista[i+1].left;
             tmp1->right = lista[i+1].right;
             lista[i].right = tmp1;             
             lista.removeAt(i+1);

             break;
         }
        }
    }

}


void descompressao::impArvore(Node *aux,  int cont){    


        if (aux[0].left==NULL && aux[0].right==NULL ){
            escrever.append(aux[0].character);

            if (cont < codificacao2.size()){
                impArvore(&lista[0],cont);
            }


        }else{

            if(codificacao2[cont]=='0'){
               cont++;
               impArvore(aux[0].left,cont);
             }else{
                if(codificacao2[cont]=='1'){
                    cont++;
                    impArvore(aux[0].right, cont);
                }

             }
        }
}

descompressao::~descompressao()
{


}
