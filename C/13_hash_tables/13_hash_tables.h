//Potencias de dois devem ser evitadas
//deve ser um numero primo distante de pequenas potencias de dois
struct item{
    int matricula;
    char nome[30];
    float n1,n2,n3;
};

typedef struct hash Hashtable;

Hashtable* createHashtable(int TABLE_SIZE);
void liberaHash(Hashtable* ha);
int valorString(char *str);
int insereHash_SemColisao(Hashtable* ha, struct item al);
int buscaHash_SemColisao(Hashtable* ha, int mat, struct item* al);
int insereHash_EnderAberto(Hashtable* ha, struct item al);
int buscaHash_EnderAberto(Hashtable* ha, int mat, struct item* al);
/*
int chaveDivisao(int chave, int TABLE_SIZE);
int chaveDobra(int chave, int TABLE_SIZE);
int chaveMultiplicacao(int chave, int TABLE_SIZE);
int sondagemLinear(int pos, int i, int TABLE_SIZE);
int sondagemQuadratica(int pos, int i, int TABLE_SIZE);
int duploHashing(int pos, int chave, int i, int TABLE_SIZE);
*/
