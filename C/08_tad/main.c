#include <stdio.h>
#include <stdlib.h>
#include "08_tad.h"
int main(){
    float d;
    Ponto *p,*q;
    //Ponto r; //ERRO
    p = pto_cria(1,7);
    q = pto_cria(5,9);
    //q->x = 2; //ERRO
    d = pto_distancia(p,q);
    printf("Distancia entre pontos: %.4f\n",d);
    pto_libera(q);
    pto_libera(p);
    system("pause");
    return 0;
}
